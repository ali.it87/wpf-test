﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PhoneBook
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadData();
        }

        void LoadData()
        {
            Thread th = new Thread(func =>
            {
                var db = new Model.AppEntity();
                var data = db.Users.ToList();
                Dispatcher.Invoke(() =>
                {
                    dgvMain.ItemsSource = null;
                    dgvMain.ItemsSource = data;
                });
            });
            th.Start();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var user = new Model.User();
            user.Firstname = txtFirstname.Text;
            user.Lastname = txtLastname.Text;
            user.Username = txtUsername.Text;
            user.Password = txtPassword.Password;
            Thread th = new Thread(func =>
            {
                var db = new Model.AppEntity();
                db.Users.Add(user);
                db.SaveChanges();
                LoadData();
            });
            th.Start();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (dgvMain.SelectedIndex == -1)
            {
                return;
            }
            if (MessageBox.Show("Are you sure?", "Delete", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                return;
            var user = dgvMain.SelectedItem as Model.User;
            if (user == null)
                return;
            Thread th = new Thread(func =>
            {
                var db = new Model.AppEntity();
                var target = db.Users.FirstOrDefault(u => u.Id == user.Id);
                if (target == null)
                    return;
                db.Users.Remove(target);
                db.SaveChanges();
                LoadData();
            });
            th.Start();
        }

        private void txtKey_TextChanged(object sender, TextChangedEventArgs e)
        {
            string key = txtKey.Text;
            if (string.IsNullOrEmpty(key)) {
                LoadData();
                return;
            }
            Thread th = new Thread(func =>
            {
                var db = new Model.AppEntity();
                var data = db.Users.Where(a => a.Username.Contains(key)).ToList();
                Dispatcher.Invoke(() =>
                {
                    dgvMain.ItemsSource = null;
                    dgvMain.ItemsSource = data;
                });
            });
            th.Start();
        }
    }
}
