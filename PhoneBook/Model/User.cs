﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Model
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(30)]
        public string Firstname { get; set; }
        [MaxLength(30)]
        public string Lastname { get; set; }
        [MaxLength(30)]
        public string Username { get; set; }
        [MaxLength(60)]
        [MinLength(5)]
        public string Password { get; set; }
    }
}
