﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Model
{
    public class AppEntity:DbContext
    {
        public DbSet<User> Users { get; set; }
    }
}
